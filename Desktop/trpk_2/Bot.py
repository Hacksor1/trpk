import telebot
import config
from telebot import types
bot = telebot.TeleBot(config.token)
print('Запущен...')
@bot.message_handler(commands=["start"])
def start(msg):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    invite = types.KeyboardButton(text = "Начинаем добавлять!")
    load = types.KeyboardButton(text = "Откуда грузить номера телефонов?")
    setting = types.KeyboardButton(text = "Настройки")
    back = types.KeyboardButton(text = "Вернуться в главное меню")
    markup.add(invite,load,setting, back)
    bot.send_message(msg.chat.id, 'Лабораторная по ТРПК №2', reply_markup=markup)

@bot.message_handler(content_types=['text']) 
def function(msg):
    if msg.text == 'Настройки':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        speed = types.KeyboardButton(text = "Установить скорость добавления")
        link = types.KeyboardButton(text = "Установить ссылку на канал")
        limit = types.KeyboardButton(text = "Установить лимит")
        back = types.KeyboardButton(text = "Вернуться в главное меню")
        markup.add(speed,link,limit,back)
        bot.send_message(msg.chat.id, 'Какие настройки?', reply_markup=markup)
    elif msg.text == 'Вернуться в главное меню':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        invite = types.KeyboardButton(text = "Начинаем добавлять!")
        load = types.KeyboardButton(text = "Откуда грузить номера телефонов?")
        setting = types.KeyboardButton(text = "Настройки")
        back = types.KeyboardButton(text = "Вернуться в главное меню")
        markup.add(invite,load,setting,back)
        bot.send_message(msg.chat.id, 'Вы вернулись в главное меню', reply_markup=markup)
    elif msg.text == 'Установить скорость добавления':
        bot.send_message(msg.chat.id, 'Укажите интервал между добавлениями (в минутах)')
    elif msg.text == 'Установить ссылку на канал':
        bot.send_message(msg.chat.id, 'Укажите id канала, куда добавлять')
    elif msg.text == 'Установить лимит':
        bot.send_message(msg.chat.id, 'Укажите лимит добавления (за сутки, не более 200)')
    elif msg.text == 'Откуда грузить номера телефонов?':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        loadExcel = types.KeyboardButton(text = "Excel")
        loadLink = types.KeyboardButton(text = "Яндекс Диск")
        loadLinkGoogle = types.KeyboardButton(text = "Google Drive")
        back = types.KeyboardButton(text = "Вернуться в главное меню")
        markup.add(loadExcel,loadLink, loadLinkGoogle, back)
        if msg.text == 'Excel':
            bot.send_message(msg.chat.id, 'Загрузите файл с расширением .xlsx')
            # получить основную информацию о файле и подготовить его к загрузке
            file_info = bot.get_file(msg.document.file_id)
            downloaded_file = bot.download_file(file_info.file_path)
    
            # определяем путь загрузки с именем файла
            src = '/src' + msg.document.file_name
            with open(src, 'wb') as new_file:
                new_file.write(downloaded_file)
        bot.send_message(msg.chat.id, 'Загрузить файл из .xlsx, Яндекс Диска или Google Drive?', reply_markup=markup)

bot.polling(none_stop=True, interval=0)
