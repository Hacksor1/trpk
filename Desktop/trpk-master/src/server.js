const { getEstimateTimeWithUsers } = require('./api_bitrix')
const { generateExcel } = require('./generate_excel')
const { uploadFile } = require('./api_yandex')
const conf = require('../conf')
const express = require('express');

const app = express()
const port = 1465
app.get('/get_report', async (req, res) => {
  const data = await getEstimateTimeWithUsers(req.query.startdate, req.query.enddate)
  const fileName = await generateExcel(data, conf.file_name)
  const fileLink = await uploadFile(fileName)
  res.setHeader('Content-Type', 'text/plain')
  res.send (fileLink)
})
app.use(express.static('public'));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
