const { default: axios } = require('axios')
const conf = require('../conf')

const rootApi = conf.bitrix_api_link

async function getAllUsers() {
  let finish = false
  let result = {}
  let nextId = 0

  while (!finish) {
    const cmd = {}
    for (let i = 0; i < 50; i++) {
      cmd[`users${i + 1}`] = `user.get?start=${nextId}`
      nextId += 50
    }
    const userBatchBody = JSON.stringify({halt: 0, cmd})
    const { data: userData } = await axios.post(`${rootApi}batch`, userBatchBody, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    let users
    let i = 0
    for (i = 0; i < 50; i++) {
      users = userData.result.result[`users${i + 1}`]
      users.map(user => {
        result[parseInt(user.ID)] = {
          name: user.NAME,
          lastName: user.LAST_NAME,
          secondName: user.SECOND_NAME
        }
      })
      if (users.length < 50) {
        finish = true
        break
      }
    }

    if (userData.result.result_total[`users${i}`] < 50 || users.length === 0) {
      finish = true
    } else {
      nextId = users[users.length - 1].ID
    }
  } 
  return result
}

async function getAllTask() {
  let finish = false
  let result = {}
  let nextId = 0

  while (!finish) {
    const cmd = {}
    for (let i = 0; i < 50; i++) {
      cmd[`tasks${i + 1}`] = `tasks.task.list?order%5BID%5D=ASC&start=${nextId}`
      nextId += 50
    }
    const taskBatchBody = JSON.stringify({halt: 0, cmd})
    const { data: taskData } = await axios.post(`${rootApi}batch`, taskBatchBody, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    let tasks
    let i = 0
    for (i = 0; i < 50; i++) {
      tasks = taskData.result.result[`tasks${i + 1}`].tasks

      tasks.map(task => {
        result[parseInt(task.id)] = {
          title: task.title,
          project: task.group.name
        }
      })
      if (tasks.length < 50) {
        finish = true
        break
      }
    }
    if (taskData.result.result_total[`tasks${i}`] < 50 || tasks.length === 0) {
      finish = true
    } else {
      nextId = tasks[tasks.length - 1].ID
    }
  }

  return result
}

async function getAllElapsedTime(startDate, endDate) {
  const dateStart = new Date(startDate).toISOString()
  let dateEnd = new Date(endDate)
  dateEnd.setDate(dateEnd.getDate() + 1)
  dateEnd = dateEnd.toISOString()
  const uriDateStart = encodeURIComponent(dateStart)
  const uriDateEnd = encodeURIComponent(dateEnd)

  let finish = false

  let result = []
  let nextId

  while (!finish) {
    const cmd = {};

    for (let i = 0; i < 50; i++) {
      if (i === 0) {
        cmd[`elapsed${i + 1}`] = `task.elapseditem.getlist?order%5BID%5D=ASC&filter%5B%3ECREATED_DATE%5D=${uriDateStart}&filter%5B%3CCREATED_DATE%5D=${uriDateEnd}&filter%5B%3EID%5D=${
          nextId || "0"
        }`;
      } else {
        cmd[`elapsed${i + 1}`] = `task.elapseditem.getlist?order%5BID%5D=ASC&filter%5B%3ECREATED_DATE%5D=${uriDateStart}&filter%5B%3CCREATED_DATE%5D=${uriDateEnd}&filter%5B%3EID%5D=%24result%5Belapsed${i}%5D%5B49%5D%5BID%5D`;
      }
    }

    const taskBatchBody = JSON.stringify({halt: 0, cmd})
    const { data: tracksData } = await axios.post(`${rootApi}batch`, taskBatchBody, {
      headers: {
        'Content-Type': 'application/json',
      },
    })

    let elapsed
    let i = 0
    for (i = 0; i < 50; i++) {
      elapsed = tracksData.result.result[`elapsed${i + 1}`]
      result.push(...elapsed.map(track => ({
        taskId: parseInt(track.TASK_ID),
        userId: parseInt(track.USER_ID),
        createdDate: track.CREATED_DATE,
        minutes: parseInt(track.MINUTES)
      })))
      if (elapsed.length < 50) {
        finish = true
        break
      }
    }

    if (tracksData.result.result_total[`elapsed${i}`] < 50 || elapsed.length === 0) {
      finish = true
    } else {
      nextId = elapsed[elapsed.length - 1].ID
    }
  }
  return result
}

async function getEstimateTimeWithUsers(startdate = '1970-01-01', enddate = '2100-01-01') {
  if (!startdate) startdate = '1970-01-01'
  if (!enddate) enddate = '2100-01-01'
  const [users, tasks, elapsedTime] = await Promise.all([
    getAllUsers(),
    getAllTask(),
    getAllElapsedTime(startdate, enddate),
  ])

  const result = []
  for (const track of elapsedTime) {
    const task = tasks[track.taskId]
    if (!task) continue
    const user = users[track.userId]
    if (!user) continue

    result.push({
      factMinutes: track.minutes,
      userId: track.userId,
      title: task.title,
      project_name: task.project,
      name: `${user.name} ${user.lastName}`,
      dateTask: track.createdDate
    })
  }
  console.log(result.length)
  return result
}

module.exports = {
  getEstimateTimeWithUsers
}