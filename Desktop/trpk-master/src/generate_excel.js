const ExcelJS = require('exceljs')


function timeFormat(seconds) {
  const hours = Math.floor(seconds / 3600)

  seconds %= 3600
  const minutes = Math.floor(seconds / 60)

  seconds %= 60

  const hoursString = hours > 99 ? hours.toString() : `0${hours}`.slice(-2)

  return `${hoursString}:${`0${minutes}`.slice(-2)}`
}

async function generateExcel(data, name) {
  const workbook = new ExcelJS.Workbook()
  const sheet = workbook.addWorksheet('My Sheet')
  sheet.columns = [
    { header: 'ФИО:', key: 'name', width: 32 },
    { header: 'Название задачи:', key: 'title', width: 32 },
    { header: 'Название проекта:', key: 'project', width: 32 },
    { header: 'Потрачено времени:', key: 'time', width: 32 },
    { header: 'Дата начала:', key: 'start', width: 32}
  ]

  const userIds = [...new Set(data.map(task => task.userId))]
  const sortDataByPeople = []
  for (const userId of userIds) {
    const userTask = data.filter(task => task.userId === userId)
    const totalMinutes = userTask.reduce((sumTime, {factMinutes}) => {
      return sumTime + parseInt(factMinutes)
    }, 0)

    sortDataByPeople.push({
      name: 'ИТОГО:',
      factMinutes: totalMinutes,
    })
    sortDataByPeople.push(...userTask)
  }

  for (const task of sortDataByPeople) {
    let start = ''
    if (task.dateTask) {
      const dateStart = new Date(task.dateTask)
      const day = `0${dateStart.getDate()}`.slice(-2)
      const month = `0${dateStart.getMonth()+1}`.slice(-2)
      start = `${day}.${month}.${dateStart.getFullYear()}`
    }
   
    sheet.insertRow(2, {
      name: task.name, 
      title: task.title, 
      project: task.project_name,
      time: timeFormat(parseInt(task.factMinutes) * 60),
      start
    })
  }

  await workbook.xlsx.writeFile(`${name}.xlsx`)
  return `${name}.xlsx`
}

module.exports = {
  generateExcel
}