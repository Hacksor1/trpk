const { default: axios } = require('axios')
const fs = require('fs')
const conf = require('../conf')

async function uploadFile(fileName) {
  try {
  const {data} = await axios.get(`https://cloud-api.yandex.net/v1/disk/resources/upload?path=trpk%2F${fileName}&overwrite=true`,{
    headers: {
      Authorization: `OAuth ${conf.yandex_token}`
    }

  })


    await axios.put(data.href, fs.createReadStream(fileName))
  }
  catch(e){
    console.log(e)
  }

  return 'https://disk.yandex.ru/d/mNg2r6OJBpCdqw'

}

module.exports = {
  uploadFile
}